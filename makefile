DESTDIR=/
BINDIR=bin/
SRCDIR=src/
INSTALL_LOCATION=$(DESTDIR)/usr/lib/
CFLAGS := -fPIC -O3 -g -Wall -Werror
CC := gcc
C_FILES := $(wildcard $(SRCDIR)*.c)
O_FILES := $(C_FILES:.c=.o)
MAJOR := 0
MINOR := 2
NAME := extend
VERSION := $(MAJOR).$(MINOR)

all: lib$(NAME).so.$(VERSION) clean
lib$(NAME).so.$(VERSION): $(O_FILES)
	$(CC) $(CFLAGS) -lm -shared -Wl,-soname,lib$(NAME).so $^ -o $(BINDIR)$@
%.o: %.c
	mkdir -p $(BINDIR)
	gcc $(CFLAGS) -lm -c -o $@ $< 
clean:
	$(RM) $(SRCDIR)*.o
install:
	cp $(BINDIR)lib$(NAME).so.$(VERSION) /usr/lib/
	cp $(SRCDIR)extend.h /usr/include/
	ln -s /usr/lib/$(BINDIR)lib$(NAME).so.$(VERSION) /usr/lib/lib$(NAME).so
	ldconfig
uninstall:
	rm /usr/lib/lib$(NAME).so
	rm /usr/lib/lib$(NAME).so.$(VERSION)
	rm /usr/include/extend.h
	ldconfig
