#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "extend.h"

	int net_available(void)
	{
		char * _HOST1 = "google.com";
		char * _HOST2 = "google.gr";
		char * _HOST3 = "microsoft.com";
		char * _HOST4 = "bing.com";
		struct hostent * server1, * server2, * server3, * server4;
		int counter=0;
		server1 = gethostbyname(_HOST1);
		server2 = gethostbyname(_HOST2);
		server3 = gethostbyname(_HOST3);
		server4 = gethostbyname(_HOST4);
		counter += server1!=NULL ? 1 :0;
		counter += server2!=NULL ? 1 :0;
		counter += server3!=NULL ? 1 :0;
		counter += server4!=NULL ? 1 :0;
		if(counter <3)return -1;
 		return 0;
	}
	
	
	char * net_request(char * host, char * port, char * path, char * method, char * data, char * header_data)
	{
		if(host == NULL)		
			return NULL;
		
		if(port == NULL)		
			port = str_merge(2,port,"80");

		if(path == NULL)		
			path = str_merge(2,path,"/");
	
		if(method == NULL)		
			method = str_merge(2,method,"GET");

		if(data == NULL)
			if((data = (char *)malloc(sizeof(char)))==NULL)		
				return NULL;

		if(header_data == NULL)
		{
			if((header_data = (char *)malloc(sizeof(char)))==NULL)		
				return NULL;
			*header_data = '\0';
		}
				
		struct hostent * server;
		struct sockaddr_in serv_addr;
		int sockfd, bytes, sent, received, total;
		char * response = NULL,* message = NULL, **header_parts = NULL, buffer[1];

		if(header_data != NULL)
			if( strstr(header_data,"=") != NULL)
				header_parts = str_split (header_data,'=');	
		
		message = str_merge(6
							,str_uppercase(method)," "
							,path," HTTP/1.0\r\nHost: "
							,host,"\r\n");
		
		if(header_parts!=NULL)
			message = str_merge(4
								,header_parts[0],": "
								,header_parts[1],"\r\n");
		
		message = str_merge(4,"Content-Type: application/json\r\nContent-Length: "
							,num_tostr((int)strlen(data)),"\r\n\r\n"
							,data
							);		

		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		
		if (sockfd < 0) 
			return NULL;
		
		server = gethostbyname(host);
		if (server == NULL)
			return NULL;
		
		memset(&serv_addr,0,sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(atoi(port));
		memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);
		
		if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
			return NULL;
		
		total = strlen(message);
		
		sent = 0;
		do 
		{
			bytes = write(sockfd,message+sent,total-sent);
			if (bytes < 0)
				return NULL;
			if (bytes == 0) 
				break;
			sent+=bytes;
		} while (sent < total);
		
		memset(buffer, 0, sizeof(buffer));
		total = sizeof(buffer)-1;
		received = 0;
		do 
		{
			bytes = recv(sockfd, buffer, 1, 0);
			if (bytes < 0)
				return NULL;
			if (bytes == 0)
				break;
			received+=bytes;
			if( (response = (char *) realloc(response,received*sizeof(char))) == NULL) 
				return NULL;
			*(response+(received-1))=buffer[0];
		} while (1);
		
		if( (response = (char *) realloc(response,(received+1)*sizeof(char))) == NULL) 
			return NULL;
		
		*(response+received)='\0';
		
		return response;
	}