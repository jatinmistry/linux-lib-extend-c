#ifndef _EXTEND_H
	#define _EXTEND_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <stdarg.h>
	#include <ctype.h>
	#include <netdb.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <unistd.h>

	typedef struct struct_args
	{
		char * 	_arg;
		int 	_level;
		int 	_data;
		void(*_function)(char *);
	}struct_args;

	extern char * str_trim		(char *);
	extern char * str_ltrim		(char *);
	extern char * str_rtrim		(char *);	
	extern char * str_merge		(int, ...);
	extern char * str_reverse	(char *);
	extern char * str_uppercase	(char *);
	extern char * str_lowercase	(char *);	
	extern char * command_exec	(char *);
	extern char * file_read		(char *);
	extern char ** str_split	(char *,char);	
	extern char * num_tostr		(int);
	extern int str_contains		(char *, char *);
	extern int file_write		(char *, char *);
	extern int file_exists		(char *);
	extern int file_append		(char *, char *);
	extern int file_create		(char *);
	extern int file_delete		(char *);
	extern int net_available	(void);

	extern void args_parsing(int, char **, struct struct_args *);

	
	double power (double , int ) __attribute__((visibility ("hidden")));
#endif