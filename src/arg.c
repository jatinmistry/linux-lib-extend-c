#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "extend.h"

void args_parsing(int _argc, char ** _argv, struct_args * _options)
{
	int i, j, countOptions = 0, countReqOptions = 0, countData = 0, countReqData = 0, countAlone = 0;

	while (strcmp(_options[countOptions++]._arg, "") != 0 && countOptions<128)
	{
		countAlone = _options[countOptions - 1]._level == 2 ? _options[countOptions - 1]._data == 1 ? 2 : 1 : countAlone;
		countData += (_options[countOptions - 1]._data != 0 && _options[countOptions - 1]._level != 2) ? 2 : 1;
		countReqOptions += _options[countOptions - 1]._level == 1 ? 1 : 0;
		countReqData += (_options[countOptions - 1]._level == 1 && _options[countOptions - 1]._data != 0) ? 1 : 0;
	}

	if (countAlone != 0)
		for (i = 1; i<_argc; i++)
		for (j = 0; j<countOptions - 1; j++)
		if (strcmp(_argv[i], _options[j]._arg) == 0 && _options[j]._level == 2)
		{
			if ((_options[j]._data != 0 && _argc != 3) || (_options[j]._data == 0 && _argc != 2))
				(_options[countOptions - 1]._function)(NULL);
			(_options[j]._function)(_argv[i + 1]);
			return;
		}

	for (i = 1; i<_argc; i++)
		for (j = 0; j<countOptions - 1; j++)
		if (strcmp(_argv[i], _options[j]._arg) == 0)
		{
			if (_options[j]._level == 1)
			{
				if (_options[j]._data != 0)
					if (++i >= _argc)
					(_options[countOptions - 1]._function)(NULL);
				countReqOptions--;
			}
			else if (_options[j]._level == 0)
			{
				if (_options[j]._data != 0)
					if (++i >= _argc)
					(_options[countOptions - 1]._function)(NULL);
			}
		}
	if (countReqOptions != 0)(_options[countOptions - 1]._function)(NULL);

	for (i = 1; i<_argc; i++)
		for (j = 0; j<countOptions - 1; j++)
		if (strcmp(_argv[i], _options[j]._arg) == 0 && _options[j]._level != 2)
		{
			if (_options[j]._data != 0)
				(_options[j]._function)(_argv[++i]);
			else
				(_options[j]._function)(NULL);
		}
}