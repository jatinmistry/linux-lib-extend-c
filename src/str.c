#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include "extend.h"

int str_contains(char * _arg0,char *_arg1)
{
	int i=0;
	char * current=_arg0;
	while(strstr(current, _arg1)!=NULL){current+=(strlen(_arg1)-1*sizeof(char)); i++;}
	return i;
}

char ** str_split(char * _arg0,char _arg1)
{
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = _arg0;
    char* last_comma = 0;
    char delim[2];
    delim[0] = _arg1;
    delim[1] = 0;
    while (*tmp)
    {
        if (_arg1 == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }
    count += last_comma < (_arg0 + strlen(_arg0) - 1);
    count++;
    if( (result = malloc(sizeof(char*) * count))==NULL) return NULL;
    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(_arg0, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }
    return result;
}

	char * str_uppercase(char * _arg0)
	{
		int i = 0;
		if (_arg0 == NULL)return NULL;
		char * _temp = (char *) malloc((strlen(_arg0)+1)*sizeof(char));
		strcpy(_temp, _arg0);	
		do
			if (*(_temp + (i*sizeof(char))) >= 97 && *(_temp + (i*sizeof(char))) <= 122)
				*(_temp + (i*sizeof(char))) -= 32;	
		 while (*(_temp + (++i)) != '\0');
		return _temp;
	}

	char * str_lowercase(char * _arg0)
	{
		int i = 0;
		if (_arg0 == NULL)return NULL;
		char * _temp = (char *) malloc((strlen(_arg0) + 1)*sizeof(char));
		strcpy(_temp, _arg0);
		do
			if (*(_temp + (i*sizeof(char))) >= 65 && *(_temp + (i*sizeof(char))) <= 90)
				*(_temp + (i*sizeof(char))) += 32;
		while (*(_temp + (++i)) != '\0');
		return _temp;
	}

	char * str_reverse(char * _arg)
	{
		int i = 0;
		char _temp;	
		if (_arg == NULL)return NULL;
		char * _temp1 = (char *) malloc((strlen(_arg)+1)*sizeof(char));
		strcpy(_temp1, _arg);
		for (i = 0; i<(strlen(_arg)) / 2; i++)
		{
			_temp = *(_temp1 + (((strlen(_temp1) - (i + 1)))*sizeof(char)));
			*(_temp1 + (((strlen(_temp1) - (i + 1)))*sizeof(char))) = *(_temp1 + i);
			*(_temp1 + (i*sizeof(char))) = _temp;
		}
		return _temp1;
	}

	char * str_ltrim(char * _arg)
	{
		if (_arg == NULL)return NULL;
		int i = 0, size = 0, j = 0;
		char * result;
		size = strlen(_arg)+1;
		while (isspace(*(_arg + (i++))));
		if ((result = (char *)malloc((1 + (size - (i--)))*sizeof(char))) == NULL) return NULL;
		while (i != size)*(result + (j++)) = *(_arg + (i++));
		*(result + j) = '\0';
		return result;
	}

	char * str_merge(int count, ...)
	{
		va_list ap;
		int i;

		int len = 1; // room for NULL
		va_start(ap, count);
		for(i=0 ; i<count ; i++)
			len += strlen(va_arg(ap, char*));
		va_end(ap);
		char *merged = calloc(sizeof(char),len);
		int null_pos = 0;

		va_start(ap, count);
		for(i=0 ; i<count ; i++)
		{
			char *s = va_arg(ap, char*);
			strcpy(merged+null_pos, s);
			null_pos += strlen(s);
		}
		va_end(ap);

		return merged;
	}

	char * str_rtrim(char * _arg0)
	{
		if (_arg0 == NULL)return NULL;
		return str_reverse(str_ltrim(str_reverse(_arg0)));
	}

	char * str_trim(char * _arg)
	{
		if (_arg == NULL)return NULL;
		return str_ltrim(str_rtrim(_arg));
	}