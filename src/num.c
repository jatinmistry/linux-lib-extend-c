#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "extend.h"

	char * num_tostr(int _arg0)
	{
		int numdigits=1;
		int position = 0;
		int temp = _arg0;
		char * _result;
		while ((temp = temp / 10) != 0)numdigits++;
		_result = (char *) malloc((numdigits)*sizeof(char));	
		do{	
			*(_result + position) = ((_arg0 - (_arg0 % (int) power(10, ((numdigits - 1) - position)))) / (int) power(10, ((numdigits - 1) - position))) +48;
			_arg0 -= (_arg0 - (_arg0 % (int) power(10, ((numdigits - 1) - position))));
		} while ((++position) != numdigits);
		*(_result + (numdigits )) = '\0';
		return _result;
	}
	
	double power (double X, int Y)
	{
		int i;
		double value = 1;
		for (i = 0; i < Y; i++)
		value *= X;
		return value;
	}